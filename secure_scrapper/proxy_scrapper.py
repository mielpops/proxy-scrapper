#!/usr/local/bin/python3

from bs4 import BeautifulSoup
from pprint import pprint
import requests

from user_agents import UserAgent

class Proxies(object):
    def __init__(self):
        self.base_url = "https://free-proxy-list.net/anonymous-proxy.html"
        self.proxy_list = []
        
    def get_proxies(self):
        """
            Get a list of tuples containing => (ipaddr, port)
            :returns:   A list of proxies tuples 
            :rtype:     list
        """
        response = requests.get(self.base_url, headers=UserAgent.MACOS_CHROME.value)
        scrapper = BeautifulSoup(response.text, "html.parser")
        table = scrapper.find('table', attrs={"id" : "proxylisttable"})

        for row in table.find('tbody').find_all("tr"):
            row = list(row)
            self.proxy_list.append(
                (row[0].get_text(), row[1].get_text())
            )
        return self.proxy_list

if __name__ == '__main__':
    proxies = Proxies().get_proxies()
    print(proxies)
